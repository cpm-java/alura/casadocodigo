package br.com.casadocodigo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.casadocodigo.DAO.ProdutoDAO;
import br.com.casadocodigo.models.CarrinhoCompra;
import br.com.casadocodigo.models.CarrinhoItem;
import br.com.casadocodigo.models.Produto;
import br.com.casadocodigo.models.TipoPreco;

@Controller
@RequestMapping("/carrinho")
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class CarrinhoComprasController {

	@Autowired
	private ProdutoDAO produtoDAO;

	@Autowired
	private CarrinhoCompra carrinho;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView add(Integer produtoId, TipoPreco tipoPreco) {
		ModelAndView mav = new ModelAndView("redirect:/produtos");

		CarrinhoItem item = criaItem(produtoId, tipoPreco);

		carrinho.add(item);

		return mav;
	}

	private CarrinhoItem criaItem(Integer produtoId, TipoPreco tipoPreco) {
		Produto produto = produtoDAO.find(produtoId);

		return new CarrinhoItem(produto, tipoPreco);
	}
}
