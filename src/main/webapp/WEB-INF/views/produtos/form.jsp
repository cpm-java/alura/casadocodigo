<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Casa do código</title>
		</head>
	<body>
		<form:form action="${ s:mvcUrl('PC#gravar').build() }" method="post" modelAttribute="produto" enctype="multipart/form-data">

			<div>
				<label for="titulo">Título</label>
				<form:input path="titulo" />
				<form:errors path="titulo"></form:errors>
			</div>
			
			<div>
				<label for="autor">Autor</label>
				<form:input path="autor"/>
				<form:errors path="autor"></form:errors>
			</div>
			
			<div>
				<label for="descricao">Descrição</label>
				<form:errors path="descricao"></form:errors>
				<form:textarea path="descricao" rows="10" cols="20"/>
			</div>
			
			<div>
				<label for="paginas">Páginas</label>
				<form:input path="paginas" type="number"/>
				<form:errors path="paginas"></form:errors>
			</div>
			
			<div>
				<label for="dataLancamento">Data de lançamento</label>
				<form:errors path="dataLancamento"></form:errors>
				<form:input path="dataLancamento"/>
			</div>
			
			<c:forEach items="${tipos}" var="tipoPreco" varStatus="status">
		
				<div>
					<label>${ tipoPreco }</label>
					<form:input type="number" path="precos[${status.index}].valor" min="0" step="0.01"/>
					<form:input type="hidden" path="precos[${status.index}].tipo" value="${ tipoPreco }"/>
				</div>
			</c:forEach>

			<div>
				<label for="sumario">Sumário</label>
				<input type="file" name="sumario">
			</div>

			<hr>
	
			<button type="submit">Cadastrar</button>
		</form:form>
	</body>
</html>