<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Casa do código</title>
		</head>
	<body>
		<h1>Lista de Produtos</h1>
		<table>
			<thead>
				<tr>
					<td>Titulo</td>
					<td>Autor</td>
					<td>Descricao</td>
					<td>Paginas</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${ produtos }" var="produto">
					<tr>
						<td><a href="${ s:mvcUrl('PC#detalhe').arg(0, produto.id).build() }">${ produto.titulo }</a></td>
						<td>${ produto.autor }</td>
						<td>${ produto.descricao }</td>
						<td>${ produto.paginas }</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<h3>${ sucesso }</h3>
	</body>
</html>